
### Подготовка 

1. Создаем VDS сервер
2. Создаем проект на GitLab
3. Подключаемся к серверу и устанавливаем:
   - docker https://www.digitalocean.com/community/tutorials/how-to-install-and-use-docker-on-ubuntu-20-04-ru
   - docker-compose https://www.digitalocean.com/community/tutorials/how-to-install-and-use-docker-compose-on-ubuntu-20-04-ru
   - gitlab runner https://docs.gitlab.com/runner/install/linux-manually.html
        - после установки устанавливаем еще для рута: gitlab-runner install --user root
        - если ошибка удаляем файл rm -rf /etc/systemd/system/gitlab-runner.service и выполняем команду еще раз
        - регистрируем ранер: gitlab-runner register; задаем для Enter an executor: shell
4. Создаем кастомный раннер в gitlab проекте: Settings->CI/CD->Runners
5. Выполняем команды на сервере: 
   - gitlab-runner verify
   - gitlab-runner restart - в gitlab раннер должен подключить (загореться зеленым)

### Настройка DEV деплоя
 
1. Добавляем Dockerfile для сборки образа из исходников через ci (в нем необходимые параметры для автоматической установки зависимостей в контейнер)
2. Создаем docker-compose.dev.yml
   - задаем сборку приложения из удаленного ораза, созданного ранером
   - указываем переменные для работы лававель которые добавляли в gitlab в environment:
3. Добавляем в gitlab дополнительные переменные из .env
4. Создаем в проекте файл .gitlab-ci.yml для конфигурации CI
   - логинимся в докер: before_script: - docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
   - прописываем скрипт для сборки образа через докер docker build --build-arg NODE_ENV="dev" -t "$REGISTRY/dev/app:$CI_COMMIT_SHA" -f ./_docker/gitlab_cicd/app/Dockerfile .
   - добавляем скрипт для сохранения образа на gitlab: docker push "$REGISTRY/dev/app:$CI_COMMIT_SHA"
   - задаем ветку при комите на которую будет выполняться скрипт CI (например only:- develop)
   - создаем стадию деплоя и прописываем необходимые команды для докера

### Настройка POD деплоя

1. Добавляем в gitlab ci/cd -> Variables переменные для прода с префиксом PROD_
2. Создаем docker-compose.prod.yml используем добавленные переменные в gitlab ci/cd -> Variables
3. Добавляем в .gitlab-ci.yml скприпт на деплой с запуском в ручном режиме по тегу
4. Пушим изменения, создаем тег в gitlab из матсер ветки, ждем сборки образа, запускаем деплой на прод вручную

